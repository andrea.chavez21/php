
  <!DOCTYPE html>
  <html lang="es">
    <head>
      <meta charset="utf-8">
      <title>Ejercicio Pirámide</title>
    </head>
    <body>
      <?php

      //Declaración de variables.
      $maximoFilas = 30;
      $asterisco = '*';
      $auxiliarRombito = 1;

      // Código para Piramide
      for( $i = 1; $i <= $maximoFilas; $i++ )
      {
        echo "<center>" . $asterisco . "</center>";
        $asterisco .= '*';
      }

      $asterisco = '*';

      echo "</br>";
      echo "</br>";
      echo "</br>";

      // Código para Rombito
      for( $i = 1; $i <= $maximoFilas / 2; $i++ )
      {
        echo "<center>" . $asterisco . "</center>";
        $asterisco .= '*';
      }
      for( $j = $maximoFilas / 2; $j >= 0; $j-- )
      {
        echo "<center>" . substr( $asterisco, $auxiliarRombito ) . "</center>";
        $auxiliarRombito++;
      }

       ?>
    </body>
  </html>
