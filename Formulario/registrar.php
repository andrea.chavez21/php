<?php
  session_start();
  if (empty($_SESSION['sesionIniciada']))
  {
  	header("Location: ./login.html");
  	exit();
  }

  if ( !array_key_exists($_POST['num_cta'], $_SESSION['Alumnos']) )
  {
    $_SESSION['Alumnos'][$_POST['num_cta']] = $_POST;
    header("Location: ./info.php");
  }

 ?>
