<?php
session_start();

if (empty($_SESSION['sesionIniciada']))
{
	header("Location: ./login.html");
	exit();
}


unset($_SESSION['sesionIniciada']);
header("Location: ./login.html")
?>
