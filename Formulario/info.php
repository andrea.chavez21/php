<?php
session_start();
if (empty($_SESSION['sesionIniciada']))
{
	header("Location: ./login.html");
	exit();
}
?>
<html>
	<head>
		<title> Info </title>
		<meta charset="utf-8">
		<link rel="stylesheet" href="./styles/style-info.css">
	</head>
	<body>
		<div class="menu">
			<ul id=”button”>
	 			<li><a href=./info.php>Inicio</a></li>
	 			<li><a href=./formulario.php>Registrar alumnos</a></li>
	 			<li><a href=./cerrar.php>Cerrar sesión</a></li>
	 		</ul>
		</div>

		<h1>Usuario autenticado</h1>

		<div class="tabla-informacion">
			<table>
				<tr>
					<th><b> <?php echo $_SESSION['Alumnos'][$_SESSION['sesionIniciada']]['nombre'] . " " . $_SESSION['Alumnos'][$_SESSION['sesionIniciada']]['primer_apellido']; ?></b></th>
					<th></th>
					<th></th>
				</tr>
				<tr>
					<td>Información</td>
				</tr>
				<tr>
					<td>Numero de cuenta: <?php echo $_SESSION['Alumnos'][$_SESSION['sesionIniciada']]['num_cta'];?></td>
				</tr>
				<tr>
					<td>Fecha de nacimiento: <?php echo $_SESSION['Alumnos'][$_SESSION['sesionIniciada']]['fecha_nac'];?></td>
				</tr>
			</table>
		</div>

		<h1>Datos guardados</h1>

		<div class="tabla-usuarios">
			<table>
				<tr>
					<th><b>#</b></th>
					<th><b>Nombre</b></th>
					<th><b>Fecha de nacimiento</b></th>
				</tr>
				<?php
					for($i=1; $i <= count($_SESSION['Alumnos']); $i++)
					{
						echo "<tr>";
						echo "<td>" . $_SESSION['Alumnos'][$i]['num_cta'] . "</td>";
						echo "<td>" . $_SESSION['Alumnos'][$i]['nombre'] . " " . $_SESSION['Alumnos'][$i]['primer_apellido'] . " " . $_SESSION['Alumnos'][$i]['segundo_apellido'] . "</td>";
						echo "<td>" . $_SESSION['Alumnos'][$i]['fecha_nac'] . "</td>";
						echo "</tr>";
					}
				 ?>

			</table>
		</div>
	</body>
</html>
