<?php
session_start();
if (empty($_SESSION['sesionIniciada']))
{
  header("Location: ./login.html");
  exit();
}
?>

<!DOCTYPE html>
<html lang="es">
  <head>
    <title>Registrar Alumno</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="./styles/style-formulario.css">
  </head>
  <body>

    <div class="menu">
			<ul id=”button”>
	 			<li><a href=./info.php>Inicio</a></li>
	 			<li><a href=./formulario.php>Registrar alumnos</a></li>
	 			<li><a href=./cerrar.php>Cerrar sesión</a></li>
	 		</ul>
		</div>

    <div class="formulario">
      <form action="./registrar.php" method="POST">

        <label for="num_cta">Número de cuenta: </label>
        <input type="number" min="1" name="num_cta" placeholder="Número de Cuenta" class="form-input" autofocus required />

        <label for="nombre">Nombre: </label>
        <input type="text" name="nombre" placeholder="Nombre" class="form-input" required />

        <label for="primer_apellido">Primero apellido: </label>
        <input type="text" name="primer_apellido" placeholder="Primer apellido" class="form-input" required />

        <label for="segundo_apellido">Segundo apellido: </label>
        <input type="text" name="segundo_apellido" placeholder="Segundo apellido" class="form-input" required />

        <label for="genero">Género: </label>
          <label class="form-radio">
            <br>
            <input type="radio" name="genero" value="M"/>
            <i class="form-icon"></i> Hombre
          </label>
          <br>
          <label class="form-radio">
            <input type="radio" name="genero" value="F"/>
            <i class="form-icon"></i> Mujer
          </label>
          <br>
          <label class="form-radio">
            <input type="radio" name="genero" value="O"/>
            <i class="form-icon"></i> Otro
          </label>
          <br>
          <br>
        <label for="fec_nac">Fecha de nacimiento: </label>
        <input type="date" name="fecha_nac" placeholder="dd/mm/aaa"class="form-input" />

        <label for="contrasena">Contraseña: </label>
        <input type="password" name="contrasena" placeholder="Contraseña" class="form-input" required/>

        <input type='submit' class="form-btn" value="Enviar"/>
      </form>
    </div>
  </body>
</html>
